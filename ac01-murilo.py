from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/calculadora', methods=['POST'])
def calcular():
    operacao = request.json['operacao']
    numeros = request.json['numeros']

    if operacao == 'soma':
        resultado = sum(numeros)
    elif operacao == 'subtracao':
        resultado = numeros[0] - sum(numeros[1:])
    elif operacao == 'multiplicacao':
        resultado = 1
        for num in numeros:
            resultado *= num
    elif operacao == 'divisao':
        resultado = numeros[0]
        for num in numeros[1:]:
            resultado /= num

    return jsonify({'resultado': resultado})

if __name__ == '__main__':
    app.run(debug=True)


#http://localhost:5000/calculadora